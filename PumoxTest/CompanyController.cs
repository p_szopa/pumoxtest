﻿using MediatR;
using System.Threading.Tasks;
using System.Web.Http;

namespace PumoxTest.WebAPI
{
    [Route("[controller]")]
    [Authorize]
    public class CompanyController : ApiController
    {
        private readonly IMediator _mediator;

        public CompanyController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Create([FromBody] CreateCompanyRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var companyResult = await _mediator.Send(new CreateCompanyCommand(request.Email, request.Name));

            return Ok();
        }

        [HttpPost]
        public async Task<IHttpActionResult> Search([FromBody] SearchCompanyRequest)
        {


            return Ok();
        }

        [HttpPut]
        [Route("/{id}")]
        public async Task<IHttpActionResult> Update(long id)
        {


            return Ok();
        }

        [HttpDelete]
        [Route("/{id}")]
        public async Task<IHttpActionResult> Delete(long id)
        {


            return Ok();
        }
    }
}
